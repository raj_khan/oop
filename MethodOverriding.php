<?php

    
//    Parent class er method jodi chold class a lekha hoy abar chold class a kichu add kore child class k instantiate kora hoy, ebong chold class er method call kora hoy tokhn r parent class er method kaj korena, rokhn child class er method execute hoy
    
    
    class DepartmentInfo{
        public $departmentName;
        public $chairman;
        public $ClassRoom;
        public $numberOfStudents;
        
        
        public function __construct($dept, $chairman, $room, $stdNumber) {
            $this->departmentName = $dept;
            $this->chairman = $chairman;
            $this->ClassRoom = $room;
            $this->numberOfStudents = $stdNumber;
        }
        
        public function departmentActivity(){
            echo 'Departmenment of ' . $this->departmentName . ' arrange a tour in every year' . '<br/>';
        }
        
    }

    
//    Ekhane parent class er DepartmentActivity method child class 2t ta te (StatisticsDept and CseDept) override kora hoyese
    
    
    class StatisticsDept extends DepartmentInfo{
        public $labsNumber;
        public function departmentActivity() {
            parent::departmentActivity();
            echo $this->departmentName .' Department publish a journal Yearly ' . '<br/>';
        }
    }
    
    
    
    class CseDept extends DepartmentInfo{
        
        public $labNumber;
        public function departmentActivity() {
            parent::departmentActivity();
            echo $this->departmentName . ' Department  recently arranged a seminar on web development' . '<br/>';
        }
        
        public function programmingActivity() {
            
            echo 'Department of ' . $this->departmentName . 'Shoud arranged programming contest like other university' . '<br/>';
            
        }
        
    }
    
    $statistics = new StatisticsDept('Statistics', 'Faruq AL Masud', 3, 50);
    
    $statistics->departmentActivity();
    $cse = new CseDept('Computer Science and Engineering', 'Fahima Sultana', 3, 100);
    $cse->departmentActivity();


?>