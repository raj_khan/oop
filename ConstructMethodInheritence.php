<?php

// 

class DepartmentInfo {

    public $departmentName;
    public $chairman;
    public $classRoom;
    public $numberOfStudents;

    public function __construct($dept, $chair, $room, $stdNumber) {
        $this->departmentName = $dept;
        $this->chairman = $chair;
        $this->classRoom = $room;
        $this->numberOfStudents = $stdNumber;
    }

    public function departmentActivity() {
        echo 'Department of ' . $this->departmentName . ' arrange a tour in evry year ' . '<br/>';
    }

}

class StaticsDept extends DepartmentInfo {

    public $labsNumber;

    public function __construct($dept, $chair, $room, $stdNumber, $labs) {
        parent::__construct($dept, $chair, $room, $stdNumber);

        $this->labsNumber = $labs;
    }

}

class CseDept extends DepartmentInfo {

    public $labsNumber;

    public function __construct($dept, $chair, $room, $stdNumber, $labs) {
        parent::__construct($dept, $chair, $room, $stdNumber);

        $this->labsNumber = $labs;
    }

    public function programmingActivity() {
        echo ' Department of ' . $this->departmentName . ' should arranged programming contset like other University' . '<br/>';
    }

}

$statics = new StaticsDept("Statics", "Md. Shahadat Hossain", 305, 50, 10);
echo $statics->labsNumber . ' Labs are avilable in ' . $statics->departmentName . ' Department' . '<br/>';
echo 'Chairman - ' . $statics->chairman . ' * Students - ' . $statics->numberOfStudents . '<br/>';
$statics->departmentActivity();


$cse = new CseDept("Computer SCience and Engineering ", " MST. Fahima Khatun ", 206, 50, 5);
echo $cse->labsNumber . ' Labs are avialble in ' . $cse->departmentName . ' Department' . '<br/>';
echo 'Chairman - ' . $cse->chairman . ' * Students -  ' . $cse->numberOfStudents . '<br/>';
$cse->departmentActivity();
$cse->programmingActivity();
?>