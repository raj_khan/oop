<?php

// jokhn amn kono bapar thake j koyek ta class er properties ba method common thake tokhn inherit kora hoy class k extend kore. ete child class base class er properties & method use korte pare abar tader nijer properties & method o use korte pare.

class DepartmentInfo {

    public $departmentName;
    public $chairman;
    public $classRoom;
    public $numberOfStudents;

    public function __construct($dept, $chair, $room, $stdNumber) {
        $this->departmentName = $dept;
        $this->chairman = $chair;
        $this->classRoom = $room;
        $this->numberOfStudents = $stdNumber;
    }

    public function departmentActivity() {
        echo 'Department of ' . $this->departmentName . ' arrange a tour in evry year ' . '<br/>';
    }

}

class StaticsDept extends DepartmentInfo {

    public $labsNumber;

}

class CseDept extends DepartmentInfo {

    public $labsNumber;

    public function programmingActivity() {
        echo 'Department of ' . $this->departmentName . 'should arranged programming contset like other University' . '<br/>';
    }

}

$statics = new StaticsDept("Statics", "Md. Shahadat Hossain", 305, 50);
echo 'Chairman - ' . $statics->chairman . ' * Students - ' . $statics->numberOfStudents . '<br/>';
$statics->departmentActivity();

$cse = new CseDept("Computer SCience and Engineering ", " MST. Fahima Khatun ", 206, 50);
echo 'Chairman - ' . $cse->chairman . ' * Students -  ' . $cse->numberOfStudents . '<br/>';
$cse->departmentActivity();
$cse->programmingActivity();
?>